#include "VM.h"

#include "commands/vmcommandend.h"
#include "commands/VMCommandLoadAX.h"
#include "commands/VMCommandStoreAX.h"
#include "commands/VMCommandIncAX.h"
#include "commands/VMCommandDecAX.h"
#include "commands/VMCommandAND.h"
#include "commands/VMCommandOR.h"
#include "commands/VMCommandXOR.h"
#include "commands/VMCommandNOT.h"
#include "commands/VMCommandAdd.h"
#include "commands/VMCommandSub.h"
#include "commands/VMCommandMul.h"
#include "commands/VMCommandDiv.h"
#include "commands/VMCommandXchg.h"
#include "commands/VMCommandCmp.h"
#include "commands/VMCommandCos.h"
#include "commands/VMCommandJz.h"
#include "commands/VMCommandExp.h"
#include "commands/VMCommandLoadPi.h"
#include "commands/VMCommandLoop.h"
#include "commands/VMCommandPow.h"
#include "commands/VMCommandResetIR.h"
#include "commands/VMCommandSin.h"
#include "commands/VMCommandLoadCX.h"
#include "commands/VMCommandStoreCX.h"
#include "commands/VMCommandNextIR.h"
#include "commands/VMCommandPush.h"
#include "commands/VMCommandPop.h"
#include "commands/VMCommandPushAX.h"
#include "commands/VMCommandPopAX.h"

template class VM<double>;

/**
 * @brief Ctor
 *
 * @param aMemorySize Size of the memory
 */
template<typename T>
VM<T>::VM(size_t aMemorySize, size_t aStackSize)
{
    mMemory = std::make_unique<VMMemory<T>>(this, aMemorySize);
    mStack = std::make_unique<VMStack<T>>(aStackSize);

    initCommandSet();
}

/**
 * @brief Dtor
 */
template<typename T>
VM<T>::~VM()
{
    foreach (VMAbstractCommand<T>* cmd, mCmdList) {
        delete cmd;
    }
}

/**
 * @brief Set limit for executed commands
 *
 * @param limit
 */
template<typename T>
void VM<T>::setInstructionsLimit(size_t aLimit)
{
    mLimit = aLimit;
}

/**
 * @brief Reset all registers to default values
 */
template<typename T>
void VM<T>::reset()
{
    mIP = mAX = mCX = mIR = 0;
    mZF = mFF = false;
}

/**
 * @brief Execute a given program
 */
template<typename T>
void VM<T>::execute(const VMProgram<T> *aProgram)
{
    reset();
    size_t iteration = 0;

    while (!mFF)
    {
        mNewIP = -1;

        uchar opCode = aProgram->getCommand(mIP).first;

        VMAbstractCommand<T>* cmd = mCmdList[opCode];

        try {
            cmd->execute(aProgram->getCommand(mIP).second);
        } catch (...) {
            // nothing
        }

        iteration++;

        if (mNewIP != -1) {
            mIP = mNewIP;
        } else {
            mIP++;
        }

        if (mLimit != 0 && iteration > mLimit) {
            break;
        }
    }
}

/**
 * @brief Add command to command set
 *
 * @param command Command to add
 */
template<typename T>
void VM<T>::addCommand(VMAbstractCommand<T>* aCommand)
{
    mCmdList[aCommand->getOpCode()] = aCommand;
}

/**
 * @brief Initialize command set
 */
template<typename T>
void VM<T>::initCommandSet()
{
    addCommand(new VMCommandEnd<double>(this));
    addCommand(new VMCommandLoadAX<double>(this));
    addCommand(new VMCommandStoreAX<double>(this));
    addCommand(new VMCommandIncAX<double>(this));
    addCommand(new VMCommandDecAX<double>(this));
    //addCommand(new VMCommandAND<double>(this));
    //addCommand(new VMCommandOR<double>(this));
    //addCommand(new VMCommandXOR<double>(this));
    //addCommand(new VMCommandNOT<double>(this));
    addCommand(new VMCommandAdd<double>(this));
    addCommand(new VMCommandSub<double>(this));
    addCommand(new VMCommandMul<double>(this));
    addCommand(new VMCommandDiv<double>(this));
    addCommand(new VMCommandXchg<double>(this));
    addCommand(new VMCommandCmp<double>(this));
    addCommand(new VMCommandCos<double>(this));
    addCommand(new VMCommandJz<double>(this));
    addCommand(new VMCommandExp<double>(this));
    addCommand(new VMCommandLoadPi<double>(this));
    addCommand(new VMCommandLoop<double>(this));
    addCommand(new VMCommandPow<double>(this));
    addCommand(new VMCommandResetIR<double>(this));
    addCommand(new VMCommandSin<double>(this));
    addCommand(new VMCommandLoadCX<double>(this));
    addCommand(new VMCommandStoreCX<double>(this));
    addCommand(new VMCommandNextIR<double>(this));
    addCommand(new VMCommandPush<double>(this));
    addCommand(new VMCommandPop<double>(this));
    addCommand(new VMCommandPushAX<double>(this));
    addCommand(new VMCommandPopAX<double>(this));
}

/**
 * @brief Returns the memory value at address aIndex
 *
 * @throws VMException If memory address is out of range
 */
template<typename T>
T& VM<T>::operator [] (size_t aIndex)
{
    setZF((*mMemory)[aIndex]);
    return (*mMemory)[aIndex];
}

/**
 * @brief Push value to the stack
 */
template<typename T>
void VM<T>::operator << (const T& aValue)
{
    setZF(aValue);
    *mStack << aValue;
}

/**
 * @brief Pop value from the stack
 */
template<typename T>
const T& VM<T>::operator >> (T& aValue)
{
    setZF(aValue);
    *mStack >> aValue;

    return aValue;
}
