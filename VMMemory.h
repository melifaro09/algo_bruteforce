#ifndef VMMEMORY_H
#define VMMEMORY_H

#include "VM.h"

template<typename T>
class VM;

/**
 * Class represent memory of the virtual machine
 *
 * @author Alexandr
 *
 * @param <T> Type of elements of the memory
 */
template<typename T>
class VMMemory {

public:
    /**
     * @brief Memory address, which will be used as the
     * address of IR-register
     */
    static const size_t IR_ADDRESS   =   0xFFFFFFFF;

    /**
     * @brief Create new {@link Memory} object
     */
    VMMemory(const VM<T>* aVM, size_t aMemSize);

    /**
     * @brief Copy-constructor
     *
     * @param aVMMemory
     */
    VMMemory(const VMMemory<T> &aVMMemory);

    /**
     * @brief Dtor
     */
    ~VMMemory();

    /**
     * @brief Returns the size of the memory
     */
    size_t size() const { return mSize; }

    /**
     * @brief Returns the value at cell with a given index
     *
     * @throws VMException If memory address is out of range
     */
    T& operator [] (size_t aIndex);

    /**
     * @brief Assign-operator for memory
     */
    VMMemory& operator=(const VMMemory& aVMMemory);

private:
    /**
     * @brief Memory array
     */
    T* mMemory;

    /**
     * @brief Size of the memory
     */
    size_t mSize;

    /**
     * @brief The VM object to use
     */
    const VM<T>* mVM;
};

#endif // VMMEMORY_H
