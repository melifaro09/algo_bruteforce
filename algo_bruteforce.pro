TEMPLATE = app

QT += qml quick
CONFIG += c++11

RESOURCES += algo_bruteforce.qrc

qml.files = algo_bruteforce.qml

launch_modeall {
	CONFIG(debug, debug|release) {
	    DESTDIR = debug
	} else {
	    DESTDIR = release
	}
}
