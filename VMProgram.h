#ifndef VMPROGRAM_H
#define VMPROGRAM_H

#include <QString>
#include <utility>
#include <uchar.h>

#include "VM.h"

/**
 * @brief VMCommand: OpCode + Argument
 */
typedef std::pair<uchar, size_t> VMCommand;

template<typename T>
class VM;

template<typename T>
class VMProgram {
private:
    /**
     * @brief Program: list of "opcode & arg" pairs
     */
    VMCommand* mProgram;

    /**
     * @brief Length of the programm
     */
    size_t mLength;

    /**
     * The {@link VM} to use
     */
    VM<T>* mVM;

public:
    /**
     * @brief Create new VMProgram object
     */
    VMProgram(VM<T>* aVM, size_t aLength);

    /**
     * @brief Returns length of the program
     */
    int getLength() const { return mLength; }

    /**
     * Set command with a given index
     *
     * @param aIndex Index of the command in program
     * @param aOpCode Opcode of the command
     * @param aArg Argument of the command
     */
    void setCommand(size_t aIndex, uchar aOpCode, size_t aArg);

    /**
     * @brief Retrun opcode of a command with a given index
     *
     * @param aIndex Index of the command
     */
    VMCommand getCommand(size_t aIndex) const { return mProgram[aIndex]; }

    /**
     * @brief Try to generate next program algorithm
     *
     * @return True if success
     */
    bool generateNext();

    /**
     * @brief Output alrorithm as a QString
     */
    QString toString() const;

private:
    /**
     * @brief Try to increment argument of a command
     * with given index
     *
     * @param aCommandIndex Index of the command
     *
     * @return True if success, otherwise false
     */
    bool incArgument(size_t aCommandIndex);

    /**
     * @brief Try to increment command OpCode with a given index
     *
     * @param aCommandIndex Index of the command
     *
     * @return True if success, otherwise false
     */
    bool incCommand(size_t aCommandIndex);
};

#endif // VMPROGRAM_H
