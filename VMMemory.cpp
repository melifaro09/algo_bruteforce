#include "VMMemory.h"

template class VMMemory<double>;

/**
 * @brief Create new {@link Memory} object
 */
template<typename T>
VMMemory<T>::VMMemory(const VM<T> *aVM, size_t aMemSize): mVM(aVM), mSize(aMemSize)
{
    mMemory = new T[mSize];
}

/**
 * @brief Dtor
 */
template<typename T>
VMMemory<T>::~VMMemory()
{
    delete [] mMemory;
}

/**
 * @brief Copy-constructor
 *
 * @param aVM
 */
template<typename T>
VMMemory<T>::VMMemory(const VMMemory<T>& aVMMemory)
{
    this->mSize = aVMMemory.mSize;
    this->mMemory = new T[mSize];
    this->mVM = aVMMemory.mVM;

    for (size_t i = 0; i < this->mSize; i++) {
        mMemory[i] = aVMMemory.mMemory[i];
    }
}

/**
 * @brief Assign-operator for memory
 */
template<typename T>
VMMemory<T>& VMMemory<T>::operator=(const VMMemory<T>& aVMMemory)
{
    this->mSize = aVMMemory.mSize;
    this->mVM = aVMMemory.mVM;

    delete [] mMemory;
    mMemory = new T[mSize];

    for (size_t i = 0; i < this->mSize; i++) {
        mMemory[i] = aVMMemory.mMemory[i];
    }

    return *this;
}

/**
 * @brief Returns the value at cell with a given index
 *
 * @details aIndex = 0xFFFF is a reference to IR-register
 *
 * @throws If memory address is out of range
 */
template<typename T>
T& VMMemory<T>::operator[](size_t aIndex) {
    if (aIndex == IR_ADDRESS) {
        aIndex = mVM->getIR();
    }

    if (aIndex < mSize) {
        return mMemory[aIndex];
    } else {
        throw "Memory error: invalid cell address";
    }
}
