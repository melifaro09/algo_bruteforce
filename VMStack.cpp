#include "VMStack.h"

template class VMStack<double>;

/**
 * @brief Create new stack object
 *
 * @param aSize - Stack size
 */
template<typename T>
VMStack<T>::VMStack(const size_t aSize): mSize(aSize), mTop(-1)
{
    mStack = new T[mSize];
}

/**
 * @brief Dtor
 */
template<typename T>
VMStack<T>::~VMStack() {
    delete [] mStack;
}

/**
 * @brief Pop element from stack
 */
template<typename T>
T VMStack<T>::pop()
{
    T res;

    if (!isEmpty()) {
        res = mStack[mTop];
        mTop--;
    } else {
        throw "Stack error: stack is empty";
    }

    return res;
}

/**
 * @brief Push element to the stack
 */
template <typename T>
void VMStack<T>::push(const T& aValue)
{
    if(isFull())
        throw "Stack error: stack overflow";

    mStack[++mTop] = aValue;
}

/**
 * @brief Returns top-element from the stack
 */
template<typename T>
const T& VMStack<T>::top() const
{
    if (isEmpty())
        throw "Stack error: stack is empty";

    return mStack[mTop];
}

/**
 * @brief Push value to the stack
 */
template<typename T>
void VMStack<T>::operator << (const T& aValue)
{
    push(aValue);
}

/**
 * @brief Pop value from the stack
 */
template<typename T>
const T& VMStack<T>::operator >> (T& aValue)
{
    aValue = pop();

    return aValue;
}
