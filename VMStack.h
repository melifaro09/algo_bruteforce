#ifndef VMSTACK_H
#define VMSTACK_H

#include<memory>

/**
 * @brief Stack class
 *
 * @author Alexandr
 */
template <typename T>
class VMStack {

private:
    /**
     * @brief Stack array
     */
    T* mStack;

    /**
     * @brief Pointer to top of the stack
     */
    size_t mTop;

    /**
     * @brief Stack size
     */
    size_t mSize;

public:
    /**
     * @brief Create new stack object
     *
     * @param aSize - Stack size
     */
    VMStack(const size_t aSize);

    /**
     * @brief Dtor
     */
    ~VMStack();

    /**
     * @brief Return true, if stack is empty, otherwise false
     */
    bool isEmpty() const { return mTop == -1; }

    /**
     * @brief Return true, if stack is full, otherwise false
     */
    bool isFull() const { return mTop == mSize - 1; }

    /**
     * @brief Pop element from stack
     */
    T pop();

    /**
     * @brief Push element to the stack
     */
    void push(const T &aValue);

    /**
     * @brief Returns top-element from the stack
     */
    const T& top() const;

    /**
     * @brief Push value to the stack
     */
    void operator << (const T& aValue);

    /**
     * @brief Pop value from the stack
     */
    const T& operator >> (T& aValue);

    /**
     * @brief Deleted operator =
     */
    VMStack& operator = (const VMStack& aVMStack) = delete;

    /**
     * @brief Deleted copy constructor
     */
    VMStack(const VMStack& aVMStack) = delete;
};

#endif // VMSTACK_H
