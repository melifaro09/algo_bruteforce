#ifndef VM_H
#define VM_H

#include <QHash>

#include "VMStack.h"
#include "VMMemory.h"
#include "VMProgram.h"
#include "commands/VMAbstractCommand.h"

/**
 * Forwarded declarations
 */
template<typename T>
class VMMemory;

template<typename T>
class VMStack;

template<typename T>
class VMAbstractCommand;

template<typename T>
class VMProgram;

/**
 * @brief Virtual machine class
 *
 * @author Alexandr
 */
template<typename T>
class VM
{
public:
    /**
     * @brief Ctor
     *
     * @param aMemorySize Size of the memory
     */
    VM(size_t aMemorySize = 1, size_t aStackSize = 256);

    /**
     * @brief Dtor
     */
    virtual ~VM();

    /**
     * @brief Set limit for executed commands
     *
     * @param limit
     */
    void setInstructionsLimit(size_t aLimit);

    /**
     * @brief Returns a value of the AX-register
     */
    T getAX() const { return mAX; }

    /**
     * @brief Sets new value for the AX-register
     */
    void setAX(T aAX) { mAX = aAX; setZF(mAX); }

    /**
     * @brief Returns a value of the CX-register
     */
    int32_t getCX() const { return mCX; }

    /**
     * @brief Sets new value for the CX-register
     */
    void setCX(int32_t aCX) { mCX = aCX; setZF(mCX); }

    /**
     * @brief Returns zero-flag
     */
    bool getZF() const { return mZF; }

    /**
     * @brief Sets zero-flag
     */
    void setZF(bool aZF) { mZF = aZF; }

    /**
     * @brief Returns a value of the IP-register
     */
    size_t getIP() const { return mIP; }

    /**
     * @brief Sets new value for the IP-register
     */
    void setIP(size_t aIP) { mNewIP = aIP; }

    /**
     * @brief Returns a value of the IR-register
     */
    size_t getIR() const { return mIR; }

    /**
     * @brief Sets new value for the IR-register
     */
    void setIR(size_t aIR) { mIR = aIR; setZF(mIR); }

    /**
     * Sets new value for the FF-flag
     */
    void setFF(bool aFF) { mFF = aFF; }

    /**
     * @brief Reset all registers to default values
     */
    void reset();

    /**
     * @brief Returns command with a given opcode
     *
     * @param aOpCode OpCode of the command
     *
     * @return The VMAbstractCommand
     */
    VMAbstractCommand<T>* getCommand(uchar aOpCode) { return mCmdList[aOpCode]; }

    /**
     * @brief Returns hash-map with all commands
     */
    const QHash<uchar, VMAbstractCommand<T>*>& getCommands() { return mCmdList; }

    /**
     * @brief Returns size of the memory
     */
    size_t getMemorySize() { return mMemory->size(); }

    /**
     * @brief Execute a given program
     */
    void execute(const VMProgram<T>* aProgram);

    /**
     * @brief Returns the memory value at address aIndex
     *
     * @throws VMException If memory address is out of range
     */
    T& operator [] (size_t aIndex);

    /**
     * @brief Push value to the stack
     */
    void operator << (const T& aValue);

    /**
     * @brief Pop value from the stack
     */
    const T& operator >> (T& aValue);

private:
    /**
     * @brief Instruction-pointer
     */
    size_t mIP;

    /**
     * @brief New value of the IP-register for JMP/LOOP-Commands
     */
    int32_t mNewIP;

    /**
     * @brief Accumulator-register of user-defined type
     */
    T mAX;

    /**
     * @brief Counter-register
     */
    int32_t mCX;

    /**
     * @brief Index-register
     */
    size_t mIR;

    /**
     * @brief Zero-flag
     */
    bool mZF;

    /**
     * @brief Programm-finish flag
     */
    bool mFF;

    /**
     * @brief Instruction limit for a program
     */
    size_t mLimit = 0;

    /**
     * @brief Internal memory of the VM
     */
    std::unique_ptr<VMMemory<T>> mMemory;

    /**
     * @brief Stack
     */
    std::unique_ptr<VMStack<T>> mStack;

    /**
     * @brief Command set of the VM
     *
     * @details Key: OpCode of the command
     */
    QHash<uchar, VMAbstractCommand<T>*> mCmdList;

    /**
     * @brief Add command to command set
     *
     * @param command Command to add
     */
    void addCommand(VMAbstractCommand<T>* aCommand);

    /**
     * @brief Initialize command set
     */
    void initCommandSet();
};

#endif // VM_H
