<h1>Bruteforce VM</h1>

<p>Simple virtual machine for bruteforce of algorithms. The virtual machine generate and execute programs with a given length and variable set of commands. It can be used to approximate of empirical data, bruteforce of algorithms etc.</p>

<h2>Virtual machine structure</h2>

<h3>Registers:</h3>
<ul>
<li><b><i>AX</i></b> - Accumulator/data register</li>
<li><b><i>CX</i></b> - Counter-register</li>
<li><b><i>IP</i></b> - Instruction-pointer</li>
<li><b><i>IR</i></b> - Index-register</li>
</ul>

<h3>Flags:</h3>
<ul>
<li><b><i>ZF</i></b> - Zero-flag</li>
<li><b><i>FF</i></b> - Finish-flag</li>
</ul>

<h3>Command set:</h3>
<ul>
<li><b><i>add ax, [mem]</i></b> - Add memory value to AX-register. Result will be stored in AX-register</li>
<li><b><i>and ax, [mem]</i></b> - Bitwise AND-operation with AX-register and memory value</li>
<li><b><i>cmp ax, [mem]</i></b> - Compare values in AX-register and memory. ZF will be set to 1, if values equals, otherwise 0</li>
<li><b><i>cos ax, [mem]</i></b> - Calculate cos() of AX-register. Result will be stored in AX-register</li>
<li><b><i>dec ax</i></b> - Decrement value at AX-register</li>
<li><b><i>div ax, [mem]</i></b> - Divide value at AX-register to memory value. Result will be stored in AX-register</li>
<li><b><i>end</i></b> - End of the program (sets FF = 1)</li>
<li><b><i>exp ax</i></b> - Calculate exp(ax). Result will be stored in AX-register</li>
<li><b><i>inc ax</i></b> - Increment value at AX-register</li>
<li><b><i>jz [address]</i></b> - Jump to command with address <i>[address]</i>, if ZF = 1, otherwise go to next command</li>
<li><b><i>load ax, [mem]</i></b> - Store value from memory to AX-register</li>
<li><b><i>load cx, [mem]</i></b> - Store value from memory to CX-register</li>
<li><b><i>loadpi</i></b> - Load π-value to AX-register</li>
<li><b><i>loop [address]</i></b> - If CX > 0 jump to command with address [address], dec CX and inc IR, otherwise go to next command</li>
<li><b><i>mul ax, [mem]</i></b> - Multiplicate value at AX-register and memory value. Result will be stored in AX-register</li>
<li><b><i>next ir</i></b> - Increment value at IR-register</li>
<li><b><i>not ax</i></b> - Bitwise NOT with value at AX-register</li>
<li><b><i>or ax, [mem]</i></b> - Bitwise OR with value at AX-register and memory value</li>
<li><b><i>pow ax, [mem]</i></b> - Calculate ax ^ [mem]. Result will be stored in AX-register</li>
<li><b><i>pop ax | [mem]</i></b> - Pop value from the stack to AX-register/memory</li>
<li><b><i>push ax | [mem]</i></b> - Push value from AX-register/memory to the stack</li>
<li><b><i>resetir</i></b> - Set IR = 0</li>
<li><b><i>sin ax</i></b> - Calculate sin() of AX-register</li>
<li><b><i>store ax, [mem]</i></b> - Store value from AX-register to memory</li>
<li><b><i>store cx, [mem]</i></b> - Store value from CX-register to memory</li>
<li><b><i>sub ax, [mem]</i></b> - Substract value at memory from AX-register. Result will be stored in AX-register</li>
<li><b><i>xchg ax, [mem]</i></b> - Exchange values in AX-register and memory</li>
<li><b><i>xor ax, [mem]</i></b> - Bitwise XOR with values at AX-register and memory value. Result will be stored in AX-register</li>
</ul>

<br>
<i><b>Note:</b> IR-register can be used as memory address in all commands</i>
<br>