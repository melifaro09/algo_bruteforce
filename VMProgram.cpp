#include "VMProgram.h"

template class VMProgram<double>;

/**
 * @brief Create new VMProgram object
 */
template<typename T>
VMProgram<T>::VMProgram(VM<T>* aVM, size_t aLength): mVM(aVM), mLength(aLength)
{
    mProgram = new VMCommand[mLength];

    for (size_t i = 0; i < mLength; i++) {
        mProgram[i].first = 0x01;                       // OpCode of "end" command
        mProgram[i].second = VMMemory<T>::IR_ADDRESS;      // Argument
    }
}

/**
 * Set command with a given index
 *
 * @param aIndex Index of the command in program
 * @param aOpCode Opcode of the command
 * @param aArg Argument of the command
 */
template<typename T>
void VMProgram<T>::setCommand(size_t aIndex, uchar aOpCode, size_t aArg)
{
    if (aIndex < mLength)
    {
        mProgram[aIndex].first = aOpCode;
        mProgram[aIndex].second = aArg;
    }
}

/**
 * @brief incArgument Try to increment argument of a command
 * with given index
 *
 * @param aCommandIndex Index of the command
 *
 * @return True if success, otherwise false
 */
template<typename T>
bool VMProgram<T>::incArgument(size_t aCommandIndex)
{
    VMAbstractCommand<T>* cmd = (VMAbstractCommand<T>*) mVM->getCommand(mProgram[aCommandIndex].first);

    if (cmd->getArgType() == E_ARG_MEMORY)
    {
        size_t memSize = mVM->getMemorySize();
        size_t oldArg = mProgram[aCommandIndex].second;

        if (oldArg == VMMemory<T>::IR_ADDRESS) {
            mProgram[aCommandIndex].second = 0;
            return true;
        }
        else if (memSize - 1 > oldArg)
        {
            mProgram[aCommandIndex].second = ++oldArg;
            return true;
        }
    }
    else if (cmd->getArgType() == E_ARG_COMMAND)
    {
        size_t oldArg = mProgram[aCommandIndex].second;

        if (mLength - 1 > oldArg)
        {
            mProgram[aCommandIndex].second = ++oldArg;
            return true;
        }
    }

    return false;
}

/**
 * @brief Try to increment command OpCode with a given index
 *
 * @param aCommandIndex Index of the command
 *
 * @return True if success, otherwise false
 */
template<typename T>
bool VMProgram<T>::incCommand(size_t aCommandIndex) {

    VMAbstractCommand<T>* cmd = (VMAbstractCommand<T>*) mVM->getCommand(mProgram[aCommandIndex].first);

    uchar opCode = cmd->getOpCode() + 1;

    VMAbstractCommand<T>* newCmd = (VMAbstractCommand<T>*)mVM->getCommand(opCode);

    if (newCmd != nullptr)
    {
        mProgram[aCommandIndex].first = newCmd->getOpCode();

        if (newCmd->getArgType() == E_ARG_MEMORY)
        {
            mProgram[aCommandIndex].second = VMMemory<T>::IR_ADDRESS;
        }
        else
        {
            mProgram[aCommandIndex].second = 0x00;
        }
        return true;
    }

    return false;
}

/**
 * @brief Try to generate next program algorithm
 *
 * @return True if success
 */
template<typename T>
bool VMProgram<T>::generateNext() {

    for (size_t i = 0; i < mLength - 1; i++) {
        if (incArgument(i)) {
            return true;
        } else if (incCommand(i)) {
            return true;
        } else {
            mProgram[i].first = 0x01;
            mProgram[i].second = VMMemory<T>::IR_ADDRESS;
        }
    }

    return false;
}

/**
 * @brief Output alrorithm as a QString
 */
template<typename T>
QString VMProgram<T>::toString() const
{
    QString result;

    result.append("=================================\n");

    for (size_t i = 0; i < mLength; i++)
    {
        VMAbstractCommand<T>* cmd = (VMAbstractCommand<T>*) mVM->getCommand(mProgram[i].first);
        size_t arg = mProgram[i].second;

        result.append(QString::number(i) + ": ");

        if (cmd->getArgType() == E_ARG_NONE)
        {
            result.append(cmd->getName() + "\n");
        }
        else if (cmd->getArgType() == E_ARG_MEMORY)
        {
            if (arg == VMMemory<T>::IR_ADDRESS)
                result.append(cmd->getName() + ", [IR]\n");
            else
                result.append(cmd->getName() + ", [" + QString::number(arg) + "]\n");
        }
        else {
            result.append(cmd->getName() + " " + QString::number(arg) + "\n");
        }
    }

    result.append("\n=================================\n");

    return result;
}
