#include "VMCommandLoadCX.h"

template class VMCommandLoadCX<double>;

/**
 * @brief Create new VMCommandLoadCX object
 */
template<typename T>
VMCommandLoadCX<T>::VMCommandLoadCX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "load cx")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandLoadCX<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandLoadCX<T>::execute(const size_t& arg)
{
    T mem = (*this->mVM)[arg];

    this->mVM->setCX((T)mem);
}
