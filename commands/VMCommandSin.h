#ifndef VMCOMMANDSIN_H
#define VMCOMMANDSIN_H

#include "VMAbstractCommand.h"

/**
 * @brief sin ax
 *
 * @details Calculate sin() of AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandSin: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandSin object
     */
    VMCommandSin(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};
#endif // VMCOMMANDSIN_H
