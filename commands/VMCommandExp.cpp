#include <math.h>
#include "VMCommandExp.h"

template class VMCommandExp<double>;

/**
 * @brief Create new VMCommandExp object
 */
template<typename T>
VMCommandExp<T>::VMCommandExp(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "exp ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandExp<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandExp<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();

    this->mVM->setAX(exp((double)ax));
}
