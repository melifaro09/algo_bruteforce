#include <math.h>
#include "VMCommandCos.h"

template class VMCommandCos<double>;

/**
 * @brief Create new VMCommandCos object
 */
template<typename T>
VMCommandCos<T>::VMCommandCos(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "cos ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandCos<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandCos<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    this->mVM->setAX(cos((double)ax));
}
