#include "VMCommandDecAX.h"

template class VMCommandDecAX<double>;

/**
 * @brief Create new VMCommandDecAX object
 */
template<typename T>
VMCommandDecAX<T>::VMCommandDecAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "dec ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandDecAX<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandDecAX<T>::execute(const size_t& arg)
{
    this->mVM->setAX(this->mVM->getAX() - 1);
}
