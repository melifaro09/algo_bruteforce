#include "VMCommandNOT.h"

/**
 * @brief Create new VMCommandNOT object
 */
template<typename T>
VMCommandNOT<T>::VMCommandNOT(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "not ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandNOT<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandNOT<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();

    this->mVM->setAX(!ax);
}
