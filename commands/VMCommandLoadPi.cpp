#include "VMCommandLoadPi.h"

template class VMCommandLoadPi<double>;

/**
 * @brief Create new VMCommandLoadPi object
 */
template<typename T>
VMCommandLoadPi<T>::VMCommandLoadPi(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "loadpi")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandLoadPi<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandLoadPi<T>::execute(const size_t& arg)
{
    this->mVM->setAX((T)3.14159265359);
}
