#ifndef VMCOMMANDLOOP_H
#define VMCOMMANDLOOP_H

#include "VMAbstractCommand.h"

/**
 * @brief loop [address]
 *
 * @details If CX > 0 jump to command with address [address],
 * dec CX and inc IR, otherwise go to next command
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandLoop: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandLoop object
     */
    VMCommandLoop(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDLOOP_H
