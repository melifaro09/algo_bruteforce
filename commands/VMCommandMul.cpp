#include "VMCommandMul.h"

template class VMCommandMul<double>;

/**
 * @brief Create new VMCommandMul object
 */
template<typename T>
VMCommandMul<T>::VMCommandMul(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "mul ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandMul<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandMul<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    this->mVM->setAX(ax * mem);
}
