#include "VMCommandJz.h"

template class VMCommandJz<double>;

/**
 * @brief Create new VMCommandJz object
 */
template<typename T>
VMCommandJz<T>::VMCommandJz(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "jz")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandJz<T>::getArgType() const
{
    return EArgType::E_ARG_COMMAND;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandJz<T>::execute(const size_t& arg)
{
    this->mVM->setIP(arg);
}
