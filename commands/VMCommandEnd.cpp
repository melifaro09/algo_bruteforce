#include "VMCommandEnd.h"

template class VMCommandEnd<double>;

/**
 * @brief Create new VMCommandEnd object
 */
template<typename T>
VMCommandEnd<T>::VMCommandEnd(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "end")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandEnd<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * @brief Returns the opcode of the command
 */
template<typename T>
uchar VMCommandEnd<T>::getOpCode() const
{
    return 0x01;        // Command end: fixed opCode
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandEnd<T>::execute(const size_t& arg)
{
    this->mVM->setFF(true);
}
