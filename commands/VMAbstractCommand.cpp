#include "VMAbstractCommand.h"

template class VMAbstractCommand<double>;

/**
 * @brief Next free OpCode number
 */
template<typename T>
uchar VMAbstractCommand<T>::sNextId = 0;

/**
 * @brief Constructor
 *
 * @details Create new {@link AbstractCommand} object with
 * automatic generierte OpCode
 *
 * @param aVM The {@link VM} to use
 * @param aName Print-name of the command
 */
template<typename T>
VMAbstractCommand<T>::VMAbstractCommand(VM<T> *aVM, QString aName): mVM(aVM), mName(aName)
{
    mOpCode = ++sNextId;
}

/**
 * @brief Dtor
 */
template<typename T>
VMAbstractCommand<T>::~VMAbstractCommand()
{
}
