#ifndef VMCOMMANDXOR_H
#define VMCOMMANDXOR_H

#include "VMAbstractCommand.h"

/**
 * @brief xor ax, [mem]
 *
 * @details Bitwise XOR with values at AX-register and memory value.
 * Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandXOR: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandXOR object
     */
    VMCommandXOR(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDXOR_H
