#include "VMCommandPushAX.h"

template class VMCommandPushAX<double>;

/**
 * @brief Create new VMCommandPushAX object
 */
template<typename T>
VMCommandPushAX<T>::VMCommandPushAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "push ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandPushAX<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandPushAX<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();

    (*this->mVM) << ax;
}
