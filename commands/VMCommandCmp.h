#ifndef VMCOMMANDCMP_H
#define VMCOMMANDCMP_H

#include "VMAbstractCommand.h"

/**
 * @brief cmp ax, [mem]
 *
 * @details Compare values in AX-register and memory.
 * ZF will be set to 1, if values equals, otherwise 0
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandCmp: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandCmp object
     */
    VMCommandCmp(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDCMP_H
