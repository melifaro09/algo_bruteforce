#include "VMCommandAdd.h"

template class VMCommandAdd<double>;

/**
 * @brief Create new VMCommandAdd object
 */
template<typename T>
VMCommandAdd<T>::VMCommandAdd(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "add ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandAdd<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandAdd<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    this->mVM->setAX(ax + mem);
}
