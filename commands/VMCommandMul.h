#ifndef VMCOMMANDMUL_H
#define VMCOMMANDMUL_H

#include "VMAbstractCommand.h"

/**
 * @brief mul ax, [mem]
 *
 * @details Multiplicate value at AX-register and memory value.
 * Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandMul: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandMul object
     */
    VMCommandMul(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDMUL_H
