#ifndef VMCOMMANDDECAX_H
#define VMCOMMANDDECAX_H

#include "VMAbstractCommand.h"

/**
 * @brief dec ax
 *
 * @details Decrement value at AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandDecAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandDecAX object
     */
    VMCommandDecAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};


#endif // VMCOMMANDDECAX_H
