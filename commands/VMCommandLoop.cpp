#include "VMCommandLoop.h"

template class VMCommandLoop<double>;

/**
 * @brief Create new VMCommandLoop object
 */
template<typename T>
VMCommandLoop<T>::VMCommandLoop(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "loop")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandLoop<T>::getArgType() const
{
    return EArgType::E_ARG_COMMAND;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandLoop<T>::execute(const size_t& arg)
{
    size_t cx = this->mVM->getCX();

    if (cx > 0) {
        cx--;
        this->mVM->setCX(cx);
        this->mVM->setIR(this->mVM->getIR());
        this->mVM->setIP(arg);
    }
}
