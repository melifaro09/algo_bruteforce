#ifndef VMCOMMANDPOPAX_H
#define VMCOMMANDPOPAX_H

#include "VMAbstractCommand.h"

/**
 * @brief pop ax
 *
 * @details Pop value from the stack to AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandPopAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandPopAX object
     */
    VMCommandPopAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDPOPAX_H
