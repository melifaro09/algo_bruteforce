#include "VMCommandNextIR.h"

template class VMCommandNextIR<double>;

/**
 * @brief Create new VMCommandNextIR object
 */
template<typename T>
VMCommandNextIR<T>::VMCommandNextIR(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "next ir")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandNextIR<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandNextIR<T>::execute(const size_t& arg)
{
    this->mVM->setIR(this->mVM->getIR() + 1);
}
