#include "VMCommandXOR.h"

/**
 * @brief Create new VMCommandXOR object
 */
template<typename T>
VMCommandXOR<T>::VMCommandXOR(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "xor ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandXOR<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandXOR<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = this->mVM[arg];

    this->mVM->setAX(ax ^ mem);
}
