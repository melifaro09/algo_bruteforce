#ifndef VMCOMMANDNEXTIR_H
#define VMCOMMANDNEXTIR_H

#include "VMAbstractCommand.h"

/**
 * @brief next ir
 *
 * @details Increment value at IR-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandNextIR: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandNextIR object
     */
    VMCommandNextIR(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDNEXTIR_H
