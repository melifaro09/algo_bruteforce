#include "VMCommandDiv.h"

template class VMCommandDiv<double>;

/**
 * @brief Create new VMCommandDiv object
 */
template<typename T>
VMCommandDiv<T>::VMCommandDiv(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "div ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandDiv<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandDiv<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    this->mVM->setAX(ax / mem);
}
