#include "VMCommandXchg.h"

template class VMCommandXchg<double>;

/**
 * @brief Create new VMCommandXchg object
 */
template<typename T>
VMCommandXchg<T>::VMCommandXchg(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "xchg ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandXchg<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandXchg<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    (*this->mVM)[arg] = ax;
    this->mVM->setAX(mem);
}
