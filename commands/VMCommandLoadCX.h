#ifndef VMCOMMANDLOADCX_H
#define VMCOMMANDLOADCX_H

#include "VMAbstractCommand.h"

/**
 * @brief load cx
 *
 * @details Store value from memory to CX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandLoadCX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandLoadCX object
     */
    VMCommandLoadCX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDLOADCX_H
