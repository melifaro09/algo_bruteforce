#include "VMCommandResetIR.h"

template class VMCommandResetIR<double>;

/**
 * @brief Create new VMCommandResetIR object
 */
template<typename T>
VMCommandResetIR<T>::VMCommandResetIR(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "reset ir")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandResetIR<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandResetIR<T>::execute(const size_t& arg)
{
    this->mVM->setIR(0);
}
