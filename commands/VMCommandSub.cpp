#include "VMCommandSub.h"

template class VMCommandSub<double>;

/**
 * @brief Create new VMCommandSub object
 */
template<typename T>
VMCommandSub<T>::VMCommandSub(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "and ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandSub<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandSub<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    this->mVM->setAX(ax - mem);
}
