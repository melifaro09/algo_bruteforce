#ifndef VMCOMMANDLOADAX_H
#define VMCOMMANDLOADAX_H

#include "VMAbstractCommand.h"

/**
 * @brief load ax
 *
 * @details Store value from memory to AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandLoadAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandLoadAX object
     */
    VMCommandLoadAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDLOADAX_H
