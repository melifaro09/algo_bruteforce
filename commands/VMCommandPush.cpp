#include "VMCommandPush.h"

template class VMCommandPush<double>;

/**
 * @brief Create new VMCommandPush object
 */
template<typename T>
VMCommandPush<T>::VMCommandPush(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "push")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandPush<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandPush<T>::execute(const size_t& arg)
{
    (*this->mVM) << (*this->mVM)[arg];
}
