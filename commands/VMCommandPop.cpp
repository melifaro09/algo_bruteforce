#include "VMCommandPop.h"

template class VMCommandPop<double>;

/**
 * @brief Create new VMCommandPop object
 */
template<typename T>
VMCommandPop<T>::VMCommandPop(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "pop")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandPop<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandPop<T>::execute(const size_t& arg)
{
    (*this->mVM) >> (*this->mVM)[arg];
}
