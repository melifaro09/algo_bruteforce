#include "VMCommandStoreAX.h"

template class VMCommandStoreAX<double>;

/**
 * @brief Create new VMCommandStoreAX object
 */
template<typename T>
VMCommandStoreAX<T>::VMCommandStoreAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "store ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandStoreAX<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandStoreAX<T>::execute(const size_t& arg)
{
    (*this->mVM)[arg] = this->mVM->getAX();
}
