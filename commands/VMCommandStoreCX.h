#ifndef VMCOMMANDSTORECX_H
#define VMCOMMANDSTORECX_H

#include "VMAbstractCommand.h"

/**
 * @brief store cx
 *
 * @details Store value from CX-register to memory
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandStoreCX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandCmp object
     */
    VMCommandStoreCX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDSTORECX_H
