#include <math.h>
#include "VMCommandPow.h"

template class VMCommandPow<double>;

/**
 * @brief Create new VMCommandPow object
 */
template<typename T>
VMCommandPow<T>::VMCommandPow(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "pow ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandPow<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandPow<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    this->mVM->setAX(pow((double)ax, (double)mem));
}
