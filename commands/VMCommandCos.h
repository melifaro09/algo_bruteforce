#ifndef VMCOMMANDCOS_H
#define VMCOMMANDCOS_H

#include "VMAbstractCommand.h"

/**
 * @brief cos ax
 *
 * @details Calculate cos() of AX-register.
 * Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandCos: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandCos object
     */
    VMCommandCos(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDCOS_H
