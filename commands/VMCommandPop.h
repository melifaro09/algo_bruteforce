#ifndef VMCOMMANDPOP_H
#define VMCOMMANDPOP_H

#include "VMAbstractCommand.h"

/**
 * @brief pop [mem]
 *
 * @details Pop value from the stack to memory address
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandPop: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandPop object
     */
    VMCommandPop(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDPOP_H
