#include "VMCommandIncAX.h"

template class VMCommandIncAX<double>;

/**
 * @brief Create new VMCommandIncAX object
 */
template<typename T>
VMCommandIncAX<T>::VMCommandIncAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "inc ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandIncAX<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandIncAX<T>::execute(const size_t& arg)
{
    this->mVM->setAX(this->mVM->getAX() + 1);
}
