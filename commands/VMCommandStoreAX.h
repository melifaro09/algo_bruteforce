#ifndef VMCOMMANDSTOREAX_H
#define VMCOMMANDSTOREAX_H

#include "VMAbstractCommand.h"

/**
 * @brief store ax
 *
 * @details Store value from AX-register to memory
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandStoreAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandStoreAX object
     */
    VMCommandStoreAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDSTOREAX_H
