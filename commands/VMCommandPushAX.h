#ifndef VMCOMMANDPUSHAX_H
#define VMCOMMANDPUSHAX_H

#include "VMAbstractCommand.h"

/**
 * @brief push ax
 *
 * @details Push value from AX-register to the stack
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandPushAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandPushAX object
     */
    VMCommandPushAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDPUSHAX_H
