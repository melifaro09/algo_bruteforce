#include "VMCommandLoadAX.h"

template class VMCommandLoadAX<double>;

/**
 * @brief Create new VMCommandLoadAX object
 */
template<typename T>
VMCommandLoadAX<T>::VMCommandLoadAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "load ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandLoadAX<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandLoadAX<T>::execute(const size_t& arg)
{
    this->mVM->setAX((*this->mVM)[arg]);
}
