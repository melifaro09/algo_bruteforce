#ifndef VMCOMMANDXCHG_H
#define VMCOMMANDXCHG_H

#include "VMAbstractCommand.h"

/**
 * @brief xchg ax, [mem]
 *
 * @details Exchange values in AX-register and memory
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandXchg: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandXchg object
     */
    VMCommandXchg(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDXCHG_H
