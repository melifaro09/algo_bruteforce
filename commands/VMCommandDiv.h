#ifndef VMCOMMANDDIV_H
#define VMCOMMANDDIV_H

#include "VMAbstractCommand.h"

/**
 * @brief div ax, [mem]
 *
 * @details Divide value at AX-register to memory value.
 * Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandDiv: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandDiv object
     */
    VMCommandDiv(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDDIV_H
