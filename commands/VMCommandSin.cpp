#include <math.h>
#include "VMCommandSin.h"

template class VMCommandSin<double>;

/**
 * @brief Create new VMCommandSin object
 */
template<typename T>
VMCommandSin<T>::VMCommandSin(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "sin ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandSin<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandSin<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();

    this->mVM->setAX(sin((double)ax));
}
