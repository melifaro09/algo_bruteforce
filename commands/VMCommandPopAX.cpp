#include "VMCommandPopAX.h"

template class VMCommandPopAX<double>;

/**
 * @brief Create new VMCommandPopAX object
 */
template<typename T>
VMCommandPopAX<T>::VMCommandPopAX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "pop ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandPopAX<T>::getArgType() const
{
    return EArgType::E_ARG_NONE;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandPopAX<T>::execute(const size_t& arg)
{
    T ax;

    (*this->mVM) >> ax;

    this->mVM->setAX(ax);
}
