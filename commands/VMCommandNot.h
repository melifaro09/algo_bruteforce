#ifndef VMCOMMANDNOT_H
#define VMCOMMANDNOT_H

#include "VMAbstractCommand.h"

/**
 * @brief not ax
 *
 * @details Bitwise NOT with value at AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandNOT: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandNOT object
     */
    VMCommandNOT(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDNOT_H
