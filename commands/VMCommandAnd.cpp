#include "VMCommandAND.h"

/**
 * @brief Create new VMCommandAND object
 */
template<typename T>
VMCommandAND<T>::VMCommandAND(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "and ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandAND<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandAND<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = this->mVM[arg];

    this->mVM->setAX(ax & mem);
}
