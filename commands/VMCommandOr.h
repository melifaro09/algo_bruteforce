#ifndef VMCOMMANDOR_H
#define VMCOMMANDOR_H

#include "VMAbstractCommand.h"

/**
 * @brief or ax, [mem]
 *
 * @details Bitwise OR with value at AX-register and memory value
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandOR: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandOR object
     */
    VMCommandOR(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDOR_H
