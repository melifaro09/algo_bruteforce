#include "VMCommandOR.h"

/**
 * @brief Create new VMCommandOR object
 */
template<typename T>
VMCommandOR<T>::VMCommandOR(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "or ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandOR<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandOR<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = this->mVM[arg];

    this->mVM->setAX(ax | mem);
}
