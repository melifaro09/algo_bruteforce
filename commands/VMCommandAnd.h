#ifndef VMCOMMANDAND_H
#define VMCOMMANDAND_H

#include "VMAbstractCommand.h"

/**
 * @brief and ax, [mem]
 *
 * @details Bitwise AND-operation with AX-register and memory value
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandAND: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandAND object
     */
    VMCommandAND(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDAND_H
