#ifndef VMCOMMANDJZ_H
#define VMCOMMANDJZ_H

#include "VMAbstractCommand.h"

/**
 * @brief jz [address]
 *
 * @details Jump to command with address [address],
 * if ZF = 1, otherwise go to next command
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandJz: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandJz object
     */
    VMCommandJz(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDJZ_H
