#ifndef VMCOMMANDLOADPI_H
#define VMCOMMANDLOADPI_H

#include "VMAbstractCommand.h"

/**
 * @brief loadpi
 *
 * @details Load π-value to AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandLoadPi: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandLoadPi object
     */
    VMCommandLoadPi(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDLOADPI_H
