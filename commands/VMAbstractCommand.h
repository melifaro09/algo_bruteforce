#ifndef VMABSTRACTCOMMAND_H
#define VMABSTRACTCOMMAND_H

#include "../VM.h"

template<typename T>
class VM;

/**
 * Type of an argument of the command
 *
 * • EArgNone - The command have no argument
 * • EArgMemory - An argument is the address of the memory
 * • EArgCommand - An argument is the address of the command
 */
enum EArgType {E_ARG_NONE, E_ARG_MEMORY, E_ARG_COMMAND};

/**
 * @brief Abstract "command" of the virtual machine
 *
 * @author Alexandr
 *
 * @param <T> Type of the AX-register and memory cells
 */
template<typename T>
class VMAbstractCommand {
public:
    /**
     * @brief Constructor
     *
     * @details Create new {@link AbstractCommand} object with
     * automatic generierte OpCode
     *
     * @param aVM The {@link VM} to use
     * @param aName Print-name of the command
     */
    VMAbstractCommand(VM<T>* aVM, QString aName);

    /**
     * @brief Dtor
     */
    virtual ~VMAbstractCommand();

    /**
     * @brief Returns the opcode of the command
     */
    virtual uchar getOpCode() const { return mOpCode; }

    /**
     * @brief Returns the name of the command
     */
    QString getName() const { return mName; }

    /**
     * @brief Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const = 0;

    /**
     * @brief Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t& arg) = 0;

protected:
    /**
     * @brief The instance of the VM to use
     */
    VM<T>* mVM;

private:
    /**
     * @brief Next free OpCode number
     */
    static uchar sNextId;

    /**
     * @brief Opcode of the command
     */
    uchar mOpCode;

    /**
     * @brief Print-name of the command
     */
    QString mName;
};

#endif // VMABSTRACTCOMMAND_H
