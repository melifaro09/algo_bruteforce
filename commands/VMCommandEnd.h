#ifndef VMCOMMANDEND_H
#define VMCOMMANDEND_H

#include "VMAbstractCommand.h"

/**
 * @brief end
 *
 * @details End of the program (sets FF = 1)
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandEnd: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandEnd object
     */
    VMCommandEnd(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * @brief Returns the opcode of the command
     */
    virtual uchar getOpCode() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDEND_H
