#ifndef VMCOMMANDPOW_H
#define VMCOMMANDPOW_H

#include "VMAbstractCommand.h"

/**
 * @brief pow ax, [mem]
 *
 * @details Calculate ax ^ [mem]. Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandPow: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandPow object
     */
    VMCommandPow(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDPOW_H
