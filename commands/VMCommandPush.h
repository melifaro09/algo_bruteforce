#ifndef VMCOMMANDPUSH_H
#define VMCOMMANDPUSH_H

#include "VMAbstractCommand.h"

/**
 * @brief push [mem]
 *
 * @details Push value from memory address to the stack
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandPush: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandPush object
     */
    VMCommandPush(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDPUSH_H
