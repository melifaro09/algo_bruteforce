#ifndef VMCOMMANDEXP_H
#define VMCOMMANDEXP_H

#include "VMAbstractCommand.h"

/**
 * @brief exp ax
 *
 * @details Calculate exp(ax). Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandExp: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandExp object
     */
    VMCommandExp(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDEXP_H
