#include "VMCommandCmp.h"

template class VMCommandCmp<double>;

/**
 * @brief Create new VMCommandCmp object
 */
template<typename T>
VMCommandCmp<T>::VMCommandCmp(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "cmp ax")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandCmp<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandCmp<T>::execute(const size_t& arg)
{
    T ax = this->mVM->getAX();
    T mem = (*this->mVM)[arg];

    if (ax == mem) {
        this->mVM->setZF(true);
    } else {
        this->mVM->setZF(false);
    }
}
