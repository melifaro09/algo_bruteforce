#ifndef VMCOMMANDSUB_H
#define VMCOMMANDSUB_H

#include "VMAbstractCommand.h"

/**
 * @brief sub ax, [mem]
 *
 * @details Substract value at memory from AX-register.
 * Result will be stored in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandSub: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandSub object
     */
    VMCommandSub(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDSUB_H
