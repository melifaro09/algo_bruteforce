#ifndef VMCOMMANDINCAX_H
#define VMCOMMANDINCAX_H

#include "VMAbstractCommand.h"

/**
 * @brief inc ax
 *
 * @details Increment value at AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandIncAX: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandIncAX object
     */
    VMCommandIncAX(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDINCAX_H
