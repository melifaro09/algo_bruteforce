#ifndef VMCOMMANDRESETIR_H
#define VMCOMMANDRESETIR_H

#include "VMAbstractCommand.h"

/**
 * @brief resetir
 *
 * @details Set IR = 0
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandResetIR: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandResetIR object
     */
    VMCommandResetIR(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDRESETIR_H
