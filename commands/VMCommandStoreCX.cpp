#include "VMCommandStoreCX.h"

template class VMCommandStoreCX<double>;

/**
 * @brief Create new VMCommandStoreCX object
 */
template<typename T>
VMCommandStoreCX<T>::VMCommandStoreCX(VM<T> *aVM):
    VMAbstractCommand<T>(aVM, "store cx")
{
}

/**
 * Returns the type of argument of the command
 *
 * @return The {@link EArgType} value
 */
template<typename T>
EArgType VMCommandStoreCX<T>::getArgType() const
{
    return EArgType::E_ARG_MEMORY;
}

/**
 * Execute the command
 *
 * @param arg Argument of the command
 *
 * @throws VMException If not executed
 */
template<typename T>
void VMCommandStoreCX<T>::execute(const size_t& arg)
{
    size_t cx = this->mVM->getCX();

    (*this->mVM)[arg] = (T)cx;
}
