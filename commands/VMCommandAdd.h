#ifndef VMCOMMANDADD_H
#define VMCOMMANDADD_H

#include "VMAbstractCommand.h"

/**
 * @brief add ax, [mem]
 *
 * @details Add memory value to AX-register. Result will be stored
 * in AX-register
 *
 * @author Alexandr
 */
template<typename T>
class VMCommandAdd: public VMAbstractCommand<T>
{
public:
    /**
     * @brief Create new VMCommandAdd object
     */
    VMCommandAdd(VM<T> *aVM);

    /**
     * Returns the type of argument of the command
     *
     * @return The {@link EArgType} value
     */
    virtual EArgType getArgType() const;

    /**
     * Execute the command
     *
     * @param arg Argument of the command
     *
     * @throws VMException If not executed
     */
    virtual void execute(const size_t &arg);
};

#endif // VMCOMMANDADD_H
