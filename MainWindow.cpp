#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "VM.h"
#include "VMStack.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnStart_clicked()
{
    VM<double> vm(1, 100);
    vm.setInstructionsLimit(50);

    ui->edOutput->clear();

    double value = ui->edInput->value();
    double result = ui->edResult->value();

    VMProgram<double> program(&vm, ui->edProgramLength->value());

    while (program.generateNext())
    {
        vm[0] = value;

        vm.execute(&program);

        if (vm.getAX() == result) {
            ui->edOutput->appendPlainText(program.toString());
        }
    }
}
