 \
commands/VMCommandXor.cpp \
commands/VMCommandPushAX.cpp \
commands/VMCommandPush.h \
commands/VMCommandPush.cpp \
commands/VMCommandPow.h \
commands/VMCommandPopAX.h \
commands/VMCommandPopAX.cpp \
commands/VMCommandPop.h \
commands/VMCommandPop.cpp \
commands/VMCommandOr.h \
commands/VMCommandOr.cpp \
commands/VMCommandNot.h \
commands/VMCommandNot.cpp \
commands/VMCommandNextIR.cpp \
commands/VMCommandMul.h \
commands/VMCommandMul.cpp \
commands/VMCommandLoop.h \
commands/VMCommandLoop.cpp \
commands/VMCommandLoadPi.h \
commands/VMCommandLoadPi.cpp \
commands/VMCommandLoadCX.h \
commands/VMCommandLoadCX.cpp \
commands/VMCommandJz.h \
commands/VMCommandJz.cpp \
commands/VMCommandExp.h \
commands/VMCommandExp.cpp \
commands/VMCommandDiv.h \
commands/VMCommandCos.h \
commands/VMCommandCos.cpp \
commands/VMCommandCmp.h \
commands/VMCommandAnd.h \
commands/VMCommandAnd.cpp \
algo_bruteforce.cpp#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T14:31:51
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AlgorithmFinder
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    VMProgram.cpp \
    VMStack.cpp \
    VMMemory.cpp \
    VM.cpp \
    commands/VMCommandStoreAX.cpp \
    commands/VMAbstractCommand.cpp \
    commands/VMCommandLoadAX.cpp \
    commands/VMCommandEnd.cpp \
    commands/VMCommandIncAX.cpp \
    commands/VMCommandDecAX.cpp \
    commands/VMCommandAdd.cpp \
    commands/VMCommandSub.cpp \
    commands/VMCommandMul.cpp \
    commands/VMCommandDiv.cpp \
    commands/VMCommandXchg.cpp \
    commands/VMCommandResetIR.cpp \
    commands/VMCommandCmp.cpp \
    commands/VMCommandJz.cpp \
    commands/VMCommandLoop.cpp \
    commands/VMCommandLoadCX.cpp \
    commands/VMCommandStoreCX.cpp \
    commands/VMCommandLoadPi.cpp \
    commands/VMCommandExp.cpp \
    commands/VMCommandSin.cpp \
    commands/VMCommandCos.cpp \
    commands/VMCommandPow.cpp \
    commands/VMCommandNextIR.cpp \
    commands/VMCommandPush.cpp \
    commands/VMCommandPop.cpp \
    commands/VMCommandPushAX.cpp \
    commands/VMCommandPopAX.cpp \
    commands/VMCommandNot.cpp \
    commands/VMCommandOr.cpp \
    commands/VMCommandAnd.cpp \
    commands/VMCommandXor.cpp

HEADERS  += MainWindow.h \
    VMProgram.h \
    VMStack.h \
    VMMemory.h \
    VM.h \
    commands/VMCommandStoreAX.h \
    commands/VMAbstractCommand.h \
    commands/VMCommandLoadAX.h \
    commands/VMCommandEnd.h \
    commands/VMCommandIncAX.h \
    commands/VMCommandDecAX.h \
    commands/VMCommandAdd.h \
    commands/VMCommandSub.h \
    commands/VMCommandMul.h \
    commands/VMCommandDiv.h \
    commands/VMCommandXchg.h \
    commands/VMCommandResetIR.h \
    commands/VMCommandCmp.h \
    commands/VMCommandJz.h \
    commands/VMCommandLoop.h \
    commands/VMCommandLoadCX.h \
    commands/VMCommandStoreCX.h \
    commands/VMCommandLoadPi.h \
    commands/VMCommandExp.h \
    commands/VMCommandSin.h \
    commands/VMCommandCos.h \
    commands/VMCommandPow.h \
    commands/VMCommandNextIR.h \
    commands/VMCommandPush.h \
    commands/VMCommandPop.h \
    commands/VMCommandPushAX.h \
    commands/VMCommandPopAX.h \
    commands/VMCommandAnd.h \
    commands/VMCommandNot.h \
    commands/VMCommandOr.h \
    commands/VMCommandXor.h

FORMS    += MainWindow.ui
